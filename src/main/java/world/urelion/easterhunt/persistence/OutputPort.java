package world.urelion.easterhunt.persistence;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import lombok.NoArgsConstructor;
import lombok.experimental.Delegate;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.jetbrains.annotations.NotNull;

/**
 * defines a {@link Material#LEVER} which will be enabled
 * if all {@link EasterEgg}s were found.
 *
 * @since 1.0.0
 */
@Slf4j
@NoArgsConstructor
@Entity
public class OutputPort {
	/**
	 * identifier to persist by complex type of {@link Location}
	 *
	 * @since 1.0.0
	 */
	@Delegate
	@EmbeddedId
	private LocationId locationId;

	/**
	 * constructor to define an {@link OutputPort} by coordinates
	 *
	 * @param world the name of the {@link World} the {@link OutputPort} is in
	 * @param x     the x-value of the {@link OutputPort}'s {@link Location}
	 * @param y     the x-value of the {@link OutputPort}'s {@link Location}
	 * @param z     the x-value of the {@link OutputPort}'s {@link Location}
	 *
	 * @since 1.0.0
	 */
	public OutputPort(
		final @NotNull String world,
		final int x,
		final int y,
		final int z
	) {
		OutputPort.log.trace("Create identifier for the output port.");
		this.locationId = new LocationId(world, x, y, z);
	}

	/**
	 * constructor to define an {@link OutputPort} by a {@link Location}
	 *
	 * @param location the {@link Location} of the {@link OutputPort}
	 *
	 * @since 1.0.0
	 */
	public OutputPort(
		final @NotNull Location location
	) {
		OutputPort.log.trace("Create identifier for the output port.");
		this.locationId = new LocationId(location);
	}
}
