package world.urelion.easterhunt.persistence;

import jakarta.persistence.Embeddable;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import proguard.annotation.KeepName;
import world.urelion.easterhunt.EasterHuntPlugin;

import java.io.Serializable;

/**
 * composite identifier of an {@link EasterEgg}
 *
 * @since 1.0.0
 */
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Embeddable
public class LocationId
implements Serializable {
	/**
	 * unique identifier for check version of {@link Serializable}
	 * {@link Class}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final long serialVersionUID = 4343752577804623761L;

	/**
	 * name of the {@link Location}'s {@link World} of the {@link EasterEgg}
	 *
	 * @since 1.0.0
	 */
	@Getter
	@Setter
	@KeepName
	private String world;
	/**
	 * x-value of the {@link EasterEgg} {@link Location}
	 *
	 * @since 1.0.0
	 */
	@Getter
	@Setter
	@KeepName
	private int    x;
	/**
	 * y-value of the {@link EasterEgg} {@link Location}
	 *
	 * @since 1.0.0
	 */
	@Getter
	@Setter
	@KeepName
	private int    y;
	/**
	 * z-value of the {@link EasterEgg} {@link Location}
	 *
	 * @since 1.0.0
	 */
	@Getter
	@Setter
	@KeepName
	private int    z;

	/**
	 * constructor to define an {@link EasterEgg} by a {@link Location}
	 *
	 * @param location the {@link Location} of the {@link EasterEgg}
	 *
	 * @throws IllegalStateException if the {@link Plugin} is not fully loaded!
	 *
	 * @since 1.0.0
	 */
	public LocationId(
		final @NotNull Location location
	)
	throws IllegalStateException {
		LocationId.log.trace("Get world from location.");
		World world = location.getWorld();

		LocationId.log.trace("Check if the location has a world.");
		if (world == null) {
			LocationId.log.trace("Get plugin instance.");
			final EasterHuntPlugin easterHuntPlugin =
				EasterHuntPlugin.getINSTANCE();
			LocationId.log.trace("Check if plugin instance is initialized.");
			if (easterHuntPlugin != null) {
				LocationId.log.warn(
					"Location has no world! Using default world."
				);
				world = easterHuntPlugin.getDefaultWorld();
			} else {
				String errorMessage =
					"Plugin not fully loaded! Couldn't get plugin instance!";
				LocationId.log.error(errorMessage);
				throw new IllegalStateException(errorMessage);
			}
		}

		LocationId.log.trace("Set world to identifier.");
		this.setWorld(world.getName());
		LocationId.log.trace("Set x-value to identifier.");
		this.setX(location.getBlockX());
		LocationId.log.trace("Set y-value to identifier.");
		this.setY(location.getBlockY());
		LocationId.log.trace("Set z-value to the identifier.");
		this.setZ(location.getBlockZ());
	}
}
