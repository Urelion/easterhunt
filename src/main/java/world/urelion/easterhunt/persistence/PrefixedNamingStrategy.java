package world.urelion.easterhunt.persistence;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.jetbrains.annotations.NotNull;
import proguard.annotation.Keep;
import world.urelion.easterhunt.ConfigProperties;
import world.urelion.easterhunt.EasterHuntPlugin;

import java.io.Serializable;

/**
 * {@link PhysicalNamingStrategy} to enable prefix for tables and sequences
 *
 * @since 1.0.0
 */
@Slf4j
@Keep
public class PrefixedNamingStrategy
extends PhysicalNamingStrategyStandardImpl {
	/**
	 * unique identifier for check version of {@link Serializable}
	 * {@link Class}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final long serialVersionUID = -4082807074819585858L;

	/**
	 * prefix, which will be added to {@link Identifier}s
	 *
	 * @since 1.0.0
	 */
	private static final @NotNull String DEFAULT_PREFIX = "EasterHunt_";

	/**
	 * adds a prefix to {@link Identifier}s
	 *
	 * @param identifier the {@link Identifier} to add the prefix to
	 *
	 * @return the prefixed {@link Identifier}
	 *
	 * @since 1.0.0
	 */
	private @NotNull Identifier addPrefix(
		final Identifier identifier
	) {
		PrefixedNamingStrategy.log.trace("Define table prefix.");
		final String prefix;

		PrefixedNamingStrategy.log.trace("Get plugin instance.");
		final EasterHuntPlugin easterHuntPlugin =
			EasterHuntPlugin.getINSTANCE();
		PrefixedNamingStrategy.log.trace(
			"Check if plugin is not successfully initialized."
		);
		if (easterHuntPlugin != null) {
			PrefixedNamingStrategy.log.trace(
				"Get table prefix from file configuration."
			);
			prefix = easterHuntPlugin.getConfig().getString(
				ConfigProperties.DB_TABLE_PREFIX
			);
		} else {
			PrefixedNamingStrategy.log.trace(
				"Plugin not successfully initialized, use default table prefix."
			);
			prefix = PrefixedNamingStrategy.DEFAULT_PREFIX;
		}

		PrefixedNamingStrategy.log.trace("Combine prefix and identifier.");
		return new Identifier(
			prefix + identifier.getText(),
			identifier.isQuoted()
		);
	}

	/**
	 * prefixes a table name
	 *
	 * @param logicalName the table name to prefix
	 * @param context     the context
	 *
	 * @return the prefixed table name
	 *
	 * @see PhysicalNamingStrategyStandardImpl#toPhysicalTableName(
	 *			Identifier,
	 *			JdbcEnvironment
	 *		)
	 *
	 * @since 1.0.0
	 */
	@Override
	public Identifier toPhysicalTableName(
		final Identifier logicalName,
		final JdbcEnvironment context
	) {
		PrefixedNamingStrategy.log.trace("Add prefix to table name.");
		return this.addPrefix(super.toPhysicalTableName(logicalName, context));
	}

	/**
	 * prefixes a sequence name
	 *
	 * @param logicalName the sequence name to prefix
	 * @param context     the context
	 *
	 * @return the prefixed sequence name
	 *
	 * @see PhysicalNamingStrategyStandardImpl#toPhysicalSequenceName(
	 *			Identifier,
	 *			JdbcEnvironment
	 *		)
	 *
	 * @since 1.0.0
	 */
	@Override
	public Identifier toPhysicalSequenceName(
		final Identifier logicalName,
		final JdbcEnvironment context
	) {
		PrefixedNamingStrategy.log.trace("Add prefix to sequence name.");
		return this.addPrefix(super.toPhysicalSequenceName(
			logicalName,
			context
		));
	}
}
