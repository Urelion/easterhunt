package world.urelion.easterhunt.persistence;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Delegate;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import proguard.annotation.KeepName;
import world.urelion.easterhunt.EasterHuntPlugin;

import java.util.UUID;

/**
 * defines an {@link EasterEgg}, which could be found by {@link Player}s
 *
 * @since 1.0.0
 */
@Slf4j
@NoArgsConstructor
@Entity
public class EasterEgg {
	/**
	 * identifier to persist by complex type of {@link Location}
	 *
	 * @since 1.0.0
	 */
	@Delegate
	@EmbeddedId
	private LocationId locationId;

	/**
	 * defines {@link UUID} of the {@link Player},
	 * who found this {@link EasterEgg}
	 * or {@link EasterHuntPlugin#EMPTY_UUID} if it is not found
	 *
	 * @since 1.0.0
	 */
	@Getter
	@KeepName
	private @NotNull UUID foundBy = EasterHuntPlugin.EMPTY_UUID;

	/**
	 * constructor to define an {@link EasterEgg} by coordinates
	 * and sets if it was already found
	 *
	 * @param world   the name of the {@link World} the {@link EasterEgg} is in
	 * @param x       the x-value of the {@link EasterEgg}'s {@link Location}
	 * @param y       the y-value of the {@link EasterEgg}'s {@link Location}
	 * @param z       the z-value of the {@link EasterEgg}'s {@link Location}
	 * @param foundBy the {@link UUID} of the {@link Player}, who found this
	 *                {@link EasterEgg} or {@code null} if no found yet
	 *
	 * @since 1.0.0
	 */
	public EasterEgg(
		final @NotNull String world,
		final int x,
		final int y,
		final int z,
		final @Nullable UUID foundBy
	) {
		EasterEgg.log.trace("Create identifier of the Easter egg.");
		this.locationId = new LocationId(world, x, y, z);

		EasterEgg.log.trace("Define if the Easter egg was already found.");
		this.setFoundBy(foundBy);
	}

	/**
	 * constructor to define an {@link EasterEgg} by coordinates
	 *
	 * @param world the name of the {@link World} the {@link EasterEgg} is in
	 * @param x     the x-value of the {@link EasterEgg}'s {@link Location}
	 * @param y     the y-value of the {@link EasterEgg}'s {@link Location}
	 * @param z     the z-value of the {@link EasterEgg}'s {@link Location}
	 *
	 * @since 1.0.0
	 */
	public EasterEgg(
		final @NotNull String world,
		final int x,
		final int y,
		final int z
	) {
		this(world, x, y, z, null);
	}

	/**
	 * constructor to define an {@link EasterEgg} by a {@link Location}
	 * and sets if it was already found
	 *
	 * @param location the {@link Location} of the {@link EasterEgg}
	 * @param foundBy  the {@link UUID} of the {@link Player}, who found this
	 *                 {@link EasterEgg} or {@code null} if no found yet
	 *
	 * @throws IllegalStateException if the {@link Plugin} is not fully loaded!
	 *
	 * @since 1.0.0
	 */
	public EasterEgg(
		final @NotNull Location location,
		final @Nullable UUID foundBy
	)
	throws IllegalStateException {
		EasterEgg.log.trace("Create identifier of the Easter egg by location.");
		this.locationId = new LocationId(location);

		EasterEgg.log.trace("Define if the Easter egg was already found.");
		this.setFoundBy(foundBy);
	}

	/**
	 * constructor to define an {@link EasterEgg} by a {@link Location}
	 *
	 * @param location the {@link Location} of the {@link EasterEgg}
	 *
	 * @throws IllegalStateException if the {@link Plugin} is not fully loaded!
	 *
	 * @since 1.0.0
	 */
	public EasterEgg(
		final @NotNull Location location
	)
	throws IllegalStateException {
		this(location, null);
	}

	/**
	 * mark this {@link EasterEgg} as found
	 *
	 * @param playerId the {@link UUID} of the {@link Player}, who found this
	 *                 {@link EasterEgg} or {@code null} if no found yet
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized or
	 *                               if the plugin is not fully loaded
	 *
	 * @since 1.0.0
	 */
	public void setFoundBy(
		final @Nullable UUID playerId
	)
	throws IllegalStateException {
		EasterEgg.log.trace("Define non-null player identifier.");
		final UUID playerUUID;
		EasterEgg.log.trace("Check if an player identifier is given.");
		if (playerId != null) {
			EasterEgg.log.trace("Set player identifier to the given one.");
			playerUUID = playerId;
		} else {
			EasterEgg.log.trace("Set player identifier to null.");
			playerUUID = EasterHuntPlugin.EMPTY_UUID;
		}

		EasterEgg.log.trace("Set the finder of this Easter egg.");
		this.foundBy = playerUUID;

		EasterEgg.log.trace("Store this egg in database.");
		Storage.storeEgg(this);
	}

	/**
	 * shows if this {@link EasterEgg} was found or not
	 *
	 * @return {@code true} if this {@link EasterEgg} was found,
	 * 		   {@code false} if not
	 *
	 * @since 1.0.0
	 */
	public boolean isFound() {
		return !(this.getFoundBy().equals(
			EasterHuntPlugin.EMPTY_UUID
		));
	}
}
