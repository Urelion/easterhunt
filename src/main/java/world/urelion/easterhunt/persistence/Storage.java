package world.urelion.easterhunt.persistence;

import jakarta.persistence.*;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import world.urelion.easterhunt.ConfigProperties;
import world.urelion.easterhunt.EasterHuntPlugin;

/*import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;*/
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * a store to persist {@link EasterEgg}s
 *
 * @since 1.0.0
 */
@Slf4j
@Singleton(style = Style.HOLDER)
public class Storage {
	/**
	 * {@link Singleton} instance of the {@link Storage}
	 *
	 * @since 1.0.0
	 */
	@Getter(AccessLevel.PRIVATE)
	private static @Nullable Storage INSTANCE;

	/**
	 * {@link Singleton} instance of an {@link EntityManagerFactory}
	 *
	 * @since 1.0.0
	 */
	@Getter(AccessLevel.PRIVATE)
	private final @NotNull EntityManagerFactory entityManagerFactory;

	/**
	 * constructor, which initialize the persistence backend connection
	 *
	 * @param config the {@link FileConfiguration}
	 *               to get the database connection settings from
	 *
	 * @since 1.0.0
	 */
	private Storage(
		final @NotNull FileConfiguration config
	) {
		Storage.log.trace("Set class loader for JPA.");
		Thread.currentThread().setContextClassLoader(
			this.getClass().getClassLoader()
		);

		Storage.log.trace(
			"Create properties map for dynamically database connection."
		);
		final Map<String, String> properties = new HashMap<>();

		Storage.log.trace("Set database driver class.");
		properties.put(
			"jakarta.persistence.jdbc.driver",
			config.getString(ConfigProperties.DB_DRIVER)
		);
		Storage.log.trace("Set hibernate dialect class.");
		properties.put(
			"hibernate.dialect",
			config.getString(ConfigProperties.DB_DIALECT)
		);
		Storage.log.trace("Set URL to database.");
		properties.put(
			"jakarta.persistence.jdbc.url",
			config.getString(ConfigProperties.DB_URL)
		);
		Storage.log.trace("Set username for database connection.");
		properties.put(
			"jakarta.persistence.jdbc.user",
			config.getString(ConfigProperties.DB_USERNAME)
		);
		Storage.log.trace("Set password for database connection.");
		properties.put(
			"jakarta.persistence.jdbc.password",
			config.getString(ConfigProperties.DB_PASSWORD)
		);

		Storage.log.trace("Initialize the EntitiyManagerFactory.");
		this.entityManagerFactory = Persistence.createEntityManagerFactory(
			"easterhunt",
			properties
		);
	}

	/**
	 * gives the {@link Storage} {@link Singleton} instance safely
	 *
	 * @return the {@link Singleton} instance of {@link Storage}
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	private static @NotNull Storage getInstanceSafely()
	throws IllegalStateException {
		Storage.log.trace("Get store instance.");
		final Storage store = Storage.getINSTANCE();
		Storage.log.trace("Check if store instance is initialized.");
		if (store == null) {
			final String errorMessage = "Store instance not initialized yet.";
			Storage.log.error(errorMessage);
			throw new IllegalStateException(errorMessage);
		}

		return store;
	}

	/**
	 * creates an {@link EntityManager} for storage operations
	 *
	 * @return the created {@link EntityManager}
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	private static @NotNull EntityManager getEntityManager()
	throws IllegalStateException {
		Storage.log.trace("Create EntityManager for backend.");
		return Storage.getInstanceSafely()
					  .getEntityManagerFactory()
					  .createEntityManager();
	}

	/**
	 * loads all {@link Entity}s, filtered by fields
	 *
	 * @param entityClass the {@link Class} of the {@link Entity}
	 *                    to get from storage
	 * @param fields      the fields to filter the {@link Entity}s to,
	 *                    if {@code null} all stored {@link Entity}s
	 *                    will be returned
	 *
	 * @return the {@link List} with all {@link Entity}s searched for
	 *
	 * @throws IllegalArgumentException if the given {@link Class}
	 *                                  is not an {@link Entity}
	 * @throws IllegalStateException    if the {@link Storage} instance
	 *                                  is not initialized
	 *
	 * @since 1.0.0
	 */
	private static <T> @NotNull List<T> loadEntitiesByFields(
		final @NotNull Class<T> entityClass,
		final @Nullable Map<String, Object> fields
	)
	throws IllegalArgumentException, IllegalStateException {
		Storage.log.trace("Check if given class is an Entity.");
		if (!(entityClass.isAnnotationPresent(Entity.class))) {
			String errorMessage = "Given class is not an Entity!";
			Storage.log.error(errorMessage);
			throw new IllegalArgumentException(errorMessage);
		}

		Storage.log.trace("Define map of search fields.");
		final Map<String, Object> fieldMap;
		Storage.log.trace("Check if given fields are null.");
		if (fields != null) {
			Storage.log.trace("Use given fields.");
			fieldMap = fields;
		} else {
			Storage.log.trace("Create new empty list of search fields.");
			fieldMap = new HashMap<>();
		}

		Storage.log.trace("Create EntityManager for backend.");
		final EntityManager entityManager = Storage.getEntityManager();

		Storage.log.trace("Get CriteriaBuilder.");
		final CriteriaBuilder criteriaBuilder =
			entityManager.getCriteriaBuilder();

		Storage.log.trace("Create CriteriaQuery.");
		final CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(
			entityClass
		);
		Storage.log.trace("Get FROM by given class.");
		final Root<T> from = criteriaQuery.from(entityClass);
		Storage.log.trace("Define SQL SELECT on query.");
		criteriaQuery.select(from);

		Storage.log.trace("Define list of WHERE clauses.");
		List<Predicate> predicates = new ArrayList<>();
		Storage.log.trace("Iterate over given fields");
		for (Entry<String, Object> field : fieldMap.entrySet()) {
			Storage.log.trace("Create WHERE clause and add it to list.");
			predicates.add(criteriaBuilder.equal(
				from.get(field.getKey()),
				field.getValue()
			));
		}
		Storage.log.trace("Check if list of WHERE clauses is empty.");
		if (!(predicates.isEmpty())) {
			Storage.log.trace("Add WHERE clauses to query.");
			criteriaQuery.where(predicates.toArray(new Predicate[0]));
			Storage.log.trace("Create TypedQuery from CriteriaQuery.");
		}

		final TypedQuery<T> typedQuery =
			entityManager.createQuery(criteriaQuery);

		Storage.log.trace("Get result set of query.");
		List<T> resultList = typedQuery.getResultList();
		Storage.log.trace("Check if result set is null.");
		if (resultList == null) {
			Storage.log.debug("Create empty result set.");
			resultList = new ArrayList<>();
		}

		Storage.log.trace("Close EntityManager.");
		entityManager.close();

		Storage.log.trace("Return result set.");
		return resultList;
	}

	/**
	 * triggers status update of all {@link OutputPort}s
	 *
	 * @return {@code true} if all {@link EasterEgg}s were found,
	 * 		   {@code false} if not
	 *
	 * @throws IllegalStateException if the plugin is not fully loaded
	 *
	 * @since 1.0.0
	 */
	private static boolean updateOutputPorts()
	throws IllegalStateException {
		Storage.log.trace("Get singleton instance of the plugin.");
		EasterHuntPlugin easterHuntPlugin = EasterHuntPlugin.getINSTANCE();
		if (easterHuntPlugin == null) {
			String errorMessage = "Plugin not fully loaded! " +
								  "Couldn't update output ports!";
			Storage.log.error(errorMessage);
			throw new IllegalStateException(errorMessage);
		}

		Storage.log.debug("Set all output ports to updated status.");
		return easterHuntPlugin.setOutputPorts();
	}

	/**
	 * initialize the {@link Singleton} instance of {@link Storage}
	 *
	 * @param config directory of the plugins' data storage
	 *
	 * @since 1.0.0
	 */
	public static void initialize(
		final @NotNull FileConfiguration config
	) {
		Storage.log.trace("Check if instance is already initialized.");
		if (Storage.getINSTANCE() != null) {
			Storage.log.warn("The instance is already initialized.");
			return;
		}

		Storage.log.debug("Create new store instance and set as singleton.");
		Storage.INSTANCE = new Storage(config);
	}

	/**
	 * gives the database type
	 *
	 * @return a {@link String} of the database type
	 *
	 * @since 1.0.0
	 */
	public static @NotNull String getDatabaseType() {
		Storage.log.trace("Define string of database type.");
		String databaseType/* = null;

		try {
			Storage.log.trace("Get connection.");
			Connection connection = Storage.getEntityManager().unwrap(
				Connection.class
			);
			Storage.log.trace("Get database type from connection.");
			databaseType = connection.getMetaData().getDatabaseProductName();
			Storage.log.trace("Close connection.");
			connection.close();
		} catch (IllegalStateException | SQLException exception) {
			Storage.log.error("Couldn't get database type!", exception);
			Storage.log.trace("Check if database type is set.");
			if (databaseType == null) {
				databaseType */= "Unknown";
				Storage.log.trace("Set database type to " + databaseType + ".");
			/*}
		}

		*/Storage.log.trace("Return database type: " + databaseType);
		return databaseType;
	}

	/**
	 * gives the database version
	 *
	 * @return a {@link String} of the database version
	 *
	 * @since 1.0.0
	 */
	public static @NotNull String getDatabaseVersion() {
		Storage.log.trace("Define string of database version.");
		String databaseVersion/* = null;

		try {
			Storage.log.trace("Get connection.");
			Connection connection = Storage.getEntityManager().unwrap(
				Connection.class
			);
			Storage.log.trace("Get meta data of the database.");
			DatabaseMetaData metaData = connection.getMetaData();
			Storage.log.trace("Get database version from meta data.");
			databaseVersion =
				metaData.getDatabaseMajorVersion() + "." +
				metaData.getDatabaseMinorVersion();
			Storage.log.trace("Close connection.");
			connection.close();
		} catch (IllegalStateException | SQLException exception) {
			Storage.log.error("Couldn't get database version!", exception);
			Storage.log.trace("Check if database version is set.");
			if (databaseVersion == null) {
				databaseVersion*/ = "Unknown";
				Storage.log.trace(
					"Set database version to " + databaseVersion + "."
				);/*
			}
		}

		*/Storage.log.trace("Return database version: " + databaseVersion);
		return databaseVersion;
	}

	/**
	 * loads all {@link EasterEgg} by the field found
	 *
	 * @param foundBy the {@link UUID} of the {@link Player}, who found this
	 *                {@link EasterEgg} or {@code null} if not found yet
	 *
	 * @return the filtered {@link List} of {@link EasterEgg}s
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static @NotNull List<EasterEgg> loadEggsFoundBy(
		final @Nullable UUID foundBy
	)
	throws IllegalStateException {
		Storage.log.trace("Define non-null player identifier.");
		final UUID playerUUID;
		Storage.log.trace("Check if an player identifier is given.");
		if (foundBy != null) {
			Storage.log.trace("Set player identifier to the given one.");
			playerUUID = foundBy;
		} else {
			Storage.log.trace("Set player identifier to null.");
			playerUUID = EasterHuntPlugin.EMPTY_UUID;
		}

		Storage.log.trace("Define list of fields to filter.");
		final Map<String, Object> fields = new HashMap<>();
		Storage.log.debug("Add filter 'foundBy=" + playerUUID + "' to list.");
		fields.put("foundBy", playerUUID);

		Storage.log.trace(
			"Request query for " +
			(foundBy != null ? "" : "not ") +
			" found Easter eggs."
		);
		return Storage.loadEntitiesByFields(EasterEgg.class, fields);
	}

	/**
	 * loads all {@link EasterEgg}s from persistence storage
	 *
	 * @return a {@link List} of all {@link EasterEgg}s in storage
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static @NotNull List<EasterEgg> loadEggs()
	throws IllegalStateException {
		Storage.log.trace("Request query without filter.");
		return Storage.loadEntitiesByFields(EasterEgg.class, null);
	}

	/**
	 * loads an {@link EasterEgg} from the store
	 *
	 * @param eggId the {@link EasterEgg} identifier
	 *
	 * @return the stored {@link EasterEgg} or {@code null} if not found
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static @Nullable EasterEgg loadEgg(
		final @NotNull LocationId eggId
	)
	throws IllegalStateException {
		Storage.log.trace("Create EntityManager for backend.");
		final EntityManager entityManager = Storage.getEntityManager();

		Storage.log.trace("Find Easter egg in store.");
		final EasterEgg egg = entityManager.find(EasterEgg.class, eggId);

		Storage.log.trace("Close EntityManager.");
		entityManager.close();

		return egg;
	}

	/**
	 * loads an {@link EasterEgg} from the store by its {@link Location}
	 *
	 * @param location the location to look for an {@link EasterEgg}
	 *
	 * @return the stored {@link EasterEgg} or {@code null} if not found
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized or
	 *                               if the plugin is not fully loaded
	 *
	 * @since 1.0.0
	 */
	public static @Nullable EasterEgg loadEgg(
		final @NotNull Location location
	)
	throws IllegalStateException {
		Storage.log.trace("Create Easter egg identifier from location.");
		return Storage.loadEgg(new LocationId(location));
	}

	/**
	 * gives the amount of {@link EasterEgg}s stored in persistence storage,
	 * filtered by fields
	 *
	 * @param foundBy the {@link UUID} of the {@link Player}, who found this
	 *                {@link EasterEgg} or {@code null} if no found yet
	 *
	 * @return the number of stored {@link EasterEgg}s
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static int countEggs(
		final @Nullable UUID foundBy
	)
	throws IllegalStateException {
		Storage.log.trace("Get the size of the result list.");
		return Storage.loadEggsFoundBy(foundBy).size();
	}

	/**
	 * gives the amount of all stored {@link EasterEgg}s
	 *
	 * @return the number of stored {@link EasterEgg}s
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static int countAllEggs()
	throws IllegalStateException {
		Storage.log.trace("Get the size of the result list.");
		return Storage.loadEggs().size();
	}

	/**
	 * gives the amount of {@link EasterEgg}s, which are not found yet
	 *
	 * @return the number of stored {@link EasterEgg}s, which are not found yet
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static int countNotFoundEggs()
	throws IllegalStateException {
		Storage.log.trace("Get the result list, filtered by 'foundBy=null'");
		return Storage.countEggs(null);
	}

	/**
	 * gives the amount of already found {@link EasterEgg}s
	 *
	 * @return the number of stored {@link EasterEgg}s, which were already found
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static int countFoundEggs()
	throws IllegalStateException {
		Storage.log.trace("Calculate amount of found Easter eggs.");
		return Storage.countAllEggs() - Storage.countNotFoundEggs();
	}

	/**
	 * gives all finders, ordered by the amount of found {@link EasterEgg}s
	 *
	 * @return a {@link LinkedHashMap} of all finders,
	 *		   on which the key set is ordered by the rank
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static @NotNull LinkedHashMap<UUID, Integer> getFinders()
	throws IllegalStateException {
		Storage.log.trace(
			"Define a list, which contains all finders " +
			"with their amount of found Easter eggs."
		);
		final Map<UUID, Integer> finderList = new HashMap<>();

		Storage.log.trace("Iterate over all stored Easter eggs.");
		for (EasterEgg egg : Storage.loadEggs()) {
			Storage.log.trace("Get UUID of the finder.");
			final UUID finderId = egg.getFoundBy();
			Storage.log.trace("Check if the Easter egg was found.");
			if (EasterHuntPlugin.EMPTY_UUID.equals(finderId)) {
				Storage.log.trace("Easter egg was not found. Ignore it.");
				continue;
			}

			Storage.log.trace("Increase amount of finder " + finderId + ".");
			finderList.put(
				finderId,
				finderList.containsKey(finderId) ?
				finderList.get(finderId) + 1 :
				1
			);
		}

		Storage.log.trace(
			"Sort list of finders by their amount of found Easter eggs."
		);
		return finderList.entrySet().stream().sorted(
			Collections.reverseOrder(Entry.comparingByValue())
		).collect(Collectors.toMap(
			Entry::getKey,
			Entry::getValue,
			(key, value) -> key,
			LinkedHashMap::new
		));
	}

	/**
	 * persists an {@link EasterEgg} in the store
	 *
	 * @param egg the {@link EasterEgg} to store
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized or
	 *                               if the plugin is not fully loaded
	 *
	 * @since 1.0.0
	 */
	public static void storeEgg(
		final @NotNull EasterEgg egg
	)
	throws IllegalStateException {
		Storage.log.trace("Create EntityManager for backend.");
		final EntityManager entityManager = Storage.getEntityManager();
		Storage.log.trace("Get transaction for backend.");
		final EntityTransaction transaction = entityManager.getTransaction();
		Storage.log.trace("Begin transaction to store an Easter egg.");
		transaction.begin();

		Storage.log.trace("Load Easter egg from persistence storage");
		EasterEgg storedEgg = Storage.loadEgg(new LocationId(
			egg.getWorld(),
			egg.getX(),
			egg.getY(),
			egg.getZ()
		));
		Storage.log.trace("Check if Easter egg is already stored.");
		if (storedEgg == null) {
			Storage.log.trace("Persist Easter egg in backend.");
			entityManager.persist(egg);
		} else {
			Storage.log.debug(
				"Easter egg already exist. Update existing one."
			);
			entityManager.merge(egg);
		}

		Storage.log.trace("Commit transaction to store an Easter egg.");
		transaction.commit();
		Storage.log.trace("Close EntityManager.");
		entityManager.close();

		Storage.log.trace("Trigger update all output ports.");
		Storage.updateOutputPorts();
	}

	/**
	 * removes an {@link EasterEgg} from the store
	 *
	 * @param eggId the identifier of the {@link EasterEgg}
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized or
	 *                               if the plugin is not fully loaded
	 * @since 1.0.0
	 */
	public static void removeEgg(
		final @NotNull LocationId eggId
	)
	throws IllegalStateException {
		Storage.log.trace("Get Easter egg by identifier.");
		final EasterEgg egg = Storage.loadEgg(eggId);
		Storage.log.trace("Check if Easter egg exists with given identifier.");
		if (egg == null) {
			Storage.log.debug(
				"No Easter egg with given identifier " +
				"found in persistence storage."
			);
			return;
		}

		Storage.log.trace("Create EntityManager for backend.");
		final EntityManager entityManager = Storage.getEntityManager();

		Storage.log.trace("Get transaction for backend.");
		EntityTransaction transaction = entityManager.getTransaction();
		Storage.log.trace("Begin transaction to remove an Easter egg.");
		transaction.begin();

		Storage.log.trace("Remove Easter egg from persistence storage.");
		entityManager.remove(
			entityManager.contains(egg) ?
			egg :
			entityManager.merge(egg)
		);

		Storage.log.trace("Commit transaction to store an Easter egg.");
		transaction.commit();
		Storage.log.trace("Close EntityManager.");
		entityManager.close();

		Storage.log.trace("Trigger update all output ports.");
		Storage.updateOutputPorts();
	}

	/**
	 * removes an {@link EasterEgg} from the store,
	 * identified by its {@link Location}
	 *
	 * @param location the {@link Location} to identify the {@link EasterEgg}
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized or
	 *                               if the plugin is not fully loaded
	 *
	 * @since 1.0.0
	 */
	public static void removeEgg(
		final @NotNull Location location
	)
	throws IllegalStateException {
		Storage.log.trace("Create Easter egg identifier from location.");
		Storage.removeEgg(new LocationId(location));
	}

	/**
	 * loads all {@link OutputPort}s from the persistence storage
	 *
	 * @return a {@link List} of all stored {@link OutputPort}s
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static @NotNull List<OutputPort> loadOutputPorts()
	throws IllegalStateException {
		Storage.log.trace("Load all output ports from persistence storage.");
		return Storage.loadEntitiesByFields(OutputPort.class, null);
	}

	/**
	 * loads an {@link OutputPort} from the store
	 *
	 * @param portId the {@link OutputPort} identifier
	 *
	 * @return the stored {@link OutputPort} or {@code null} if not found
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static @Nullable OutputPort loadOutputPort(
		final @NotNull LocationId portId
	)
	throws IllegalStateException {
		Storage.log.trace("Create EntityManager for backend.");
		final EntityManager entityManager = Storage.getEntityManager();

		Storage.log.trace("Find output port in store.");
		final OutputPort port = entityManager.find(OutputPort.class, portId);

		Storage.log.trace("Close EntityManager.");
		entityManager.close();

		return port;
	}

	/**
	 * loads an {@link OutputPort} from the store by its {@link Location}
	 *
	 * @param location the location to look for an {@link OutputPort}
	 *
	 * @return the stored {@link OutputPort} or {@code null} if not found
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static @Nullable OutputPort loadOutputPort(
		final @NotNull Location location
	)
	throws IllegalStateException {
		Storage.log.trace("Create output port identifier from location.");
		return Storage.loadOutputPort(new LocationId(location));
	}

	/**
	 * persists an {@link OutputPort} in the store
	 *
	 * @param port the {@link OutputPort} to store
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized or
	 *                               if the plugin is not fully loaded
	 *
	 * @since 1.0.0
	 */
	public static void storeOutputPort(
		final @NotNull OutputPort port
	)
	throws IllegalStateException {
		Storage.log.trace("Create EntityManager for backend.");
		final EntityManager entityManager = Storage.getEntityManager();
		Storage.log.trace("Get transaction for backend.");
		final EntityTransaction transaction = entityManager.getTransaction();
		Storage.log.trace("Begin transaction to store an output port.");
		transaction.begin();

		Storage.log.trace("Load output port from persistence storage");
		OutputPort storedPort = Storage.loadOutputPort(new LocationId(
			port.getWorld(),
			port.getX(),
			port.getY(),
			port.getZ()
		));
		Storage.log.trace("Check if output port is already stored.");
		if (storedPort == null) {
			Storage.log.trace("Persist output port in backend.");
			entityManager.persist(port);
		} else {
			Storage.log.debug(
				"Output port already exist. Update existing one."
			);
			entityManager.merge(port);
		}

		Storage.log.trace("Commit transaction to store an output port.");
		transaction.commit();
		Storage.log.trace("Close EntityManager.");
		entityManager.close();

		Storage.log.trace("Trigger update all output ports.");
		Storage.updateOutputPorts();
	}

	/**
	 * removes an {@link OutputPort} from the store
	 *
	 * @param portId the identifier of the {@link OutputPort}
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static void removeOutputPort(
		final @NotNull LocationId portId
	)
	throws IllegalStateException {
		Storage.log.trace("Get output port by identifier.");
		final OutputPort port = Storage.loadOutputPort(portId);
		Storage.log.trace(
			"Check if output port exists with given identifier."
		);
		if (port == null) {
			Storage.log.debug(
				"No output port with given identifier " +
				"found in persistence storage."
			);
			return;
		}

		Storage.log.trace("Create EntityManager for backend.");
		final EntityManager entityManager = Storage.getEntityManager();

		Storage.log.trace("Get transaction for backend.");
		EntityTransaction transaction = entityManager.getTransaction();
		Storage.log.trace("Begin transaction to remove an output port.");
		transaction.begin();

		Storage.log.trace("Remove output port from persistence storage.");
		entityManager.remove(
			entityManager.contains(port) ?
			port :
			entityManager.merge(port)
		);

		Storage.log.trace("Commit transaction to store an output port.");
		transaction.commit();
		Storage.log.trace("Close EntityManager.");
		entityManager.close();
	}

	/**
	 * removes an {@link OutputPort} from the store,
	 * identified by its {@link Location}
	 *
	 * @param location the {@link Location} to identify the {@link OutputPort}
	 *
	 * @throws IllegalStateException if the {@link Storage} instance
	 *                               is not initialized
	 *
	 * @since 1.0.0
	 */
	public static void removeOutputPort(
		final @NotNull Location location
	)
	throws IllegalStateException {
		Storage.log.trace("Create output port identifier from location.");
		Storage.removeOutputPort(new LocationId(location));
	}
}
