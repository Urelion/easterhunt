package world.urelion.easterhunt;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.jetbrains.annotations.NotNull;
import world.urelion.easterhunt.persistence.EasterEgg;
import world.urelion.easterhunt.persistence.OutputPort;
import world.urelion.easterhunt.persistence.Storage;

/**
 * {@link Listener} to be triggered
 * by break an {@link EasterEgg} or an {@link OutputPort}
 *
 * @since 1.0.0
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Singleton(style = Style.HOLDER)
public class BlockListener
implements Listener {
	/**
	 * {@link Singleton} instance of the {@link BlockListener}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final @NotNull BlockListener INSTANCE = new BlockListener();

	/**
	 * process the deletion of an {@link EasterEgg} by breaking the block
	 *
	 * @param event the {@link BlockBreakEvent},
	 *              which is fired if an {@link Block} was broken
	 *
	 * @since 1.0.0
	 */
	@EventHandler
	public void onEggBreak(
		final BlockBreakEvent event
	) {
		BlockListener.log.trace("Get player from event.");
		Player player = event.getPlayer();

		BlockListener.log.trace("Get location of the broken block.");
		Location location = event.getBlock().getLocation();

		BlockListener.log.trace(
			"Define an Easter egg, stored at the location of the broken block."
		);
		final EasterEgg egg;
		BlockListener.log.trace(
			"Define an output port, stored at the location of the broken block."
		);
		final OutputPort port;
		try {
			BlockListener.log.trace("Load Easter egg from block location.");
			egg = Storage.loadEgg(location);
			BlockListener.log.trace("Load output port from block location.");
			port = Storage.loadOutputPort(location);
		} catch (IllegalStateException exception) {
			BlockListener.log.error(
				"Couldn't load data from persistence storage!",
				exception
			);
			return;
		}

		BlockListener.log.trace(
			"Check if the block was an Easter egg or an output port."
		);
		if (egg == null && port == null) {
			BlockListener.log.debug(
				"Broken block was not an Easter egg nor an output port. " +
				"Abort event execution."
			);
			return;
		}

		BlockListener.log.trace("Get plugin instance.");
		final EasterHuntPlugin easterHuntPlugin =
			EasterHuntPlugin.getINSTANCE();
		BlockListener.log.trace("Check if plugin instance is initialized.");
		if (easterHuntPlugin == null) {
			BlockListener.log.error(
				"Plugin not fully loaded! Abort event execution."
			);
			return;
		}

		BlockListener.log.trace("Get Vault permissions handler.");
		final Permission permissions = easterHuntPlugin.getPermissions();
		if (permissions == null) {
			BlockListener.log.trace(
				"Send player information about missing permissions handler."
			);
			player.sendMessage(
				"Couldn't check permissions! Is Vault installed?"
			);

			BlockListener.log.error(
				"Vault permissions handler not initialized! " +
				"Abort event execution."
			);
			return;
		}

		BlockListener.log.trace("Check if broken block was an Easter egg.");
		if (egg != null) {
			BlockListener.log.trace(
				"Check if player has permission to break an Easter egg."
			);
			if (!(permissions.has(
				player,
				EasterHuntPlugin.COMMAND_NAME + ".egg.break"
			))) {
				BlockListener.log.warn(
					"Player " + player.getName() +
					" has no permission to break an Easter egg."
				);
				player.sendMessage(EasterHuntPlugin.MISSING_PERMISSION_TEXT);

				BlockListener.log.info(
					"Missing permission. Cancel BlockBreakEvent."
				);
				event.setCancelled(true);

				return;
			}

			try {
				BlockListener.log.trace(
					"Remove Easter egg, because the block was broken."
				);
				Storage.removeEgg(location);

				BlockListener.log.trace(
					"Send feedback to player about Easter egg breaking."
				);
				player.sendMessage(
					"You broke an " +
					ChatColor.GOLD + "Easter egg" + ChatColor.RESET + ".",
					"There are " + Storage.countNotFoundEggs() + " " +
					ChatColor.GOLD + "Easter egg(s)" + ChatColor.RESET +
					" left in the world."
				);
			} catch (IllegalStateException exception) {
				BlockListener.log.error(
					"Couldn't remove Easter egg from persistence storage!",
					exception
				);
				return;
			}
		}

		if (port != null) {
			if (!(permissions.has(
				player,
				EasterHuntPlugin.COMMAND_NAME + ".output.break"
			))) {
				BlockListener.log.warn(
					"Player " + player.getName() +
					" has no permission to break an output port."
				);
				player.sendMessage(EasterHuntPlugin.MISSING_PERMISSION_TEXT);

				BlockListener.log.info(
					"Missing permission. Cancel BlockBreakEvent."
				);
				event.setCancelled(true);

				return;
			}

			try {
				BlockListener.log.trace(
					"Remove output port, because the block was broken."
				);
				Storage.removeOutputPort(location);
			} catch (IllegalStateException exception) {
				BlockListener.log.error(
					"Couldn't remove output port from persistence storage!",
					exception
				);
				return;
			}

			BlockListener.log.trace(
				"Send feedback to player about output port breaking."
			);
			player.sendMessage(
				"You broke an " +
				ChatColor.RED + "output port" + ChatColor.RESET + "."
			);
		}
	}
}
