package world.urelion.easterhunt;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * {@link Item}s to give the {@link Player} for hide or seek and to start
 * process on interaction
 *
 * @since 1.0.0
 */
@Slf4j
public class HunterItem {
	/**
	 * the default {@link Enchantment} to make the {@link HunterItem}s
	 * <i>shiny</i>
	 *
	 * @since 1.0.0
	 */
	private static final @NotNull Enchantment ENCHANTMENT = Enchantment.MENDING;

	/**
	 * {@link Material} of {@link Item} to use for hide or seek<br>
	 * Should be an {@link Item},
	 * which has not a default effect on interact with the world.
	 *
	 * @since 1.0.0
	 */
	@Getter
	private final @NotNull Material material;

	/**
	 * name or title to show on hover the {@link Item}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private final @NotNull String name;

	/**
	 * lore with description how to use<br>
	 * will be shown on hover the {@link Item}
	 *
	 * @since 1.0.0
	 */
	private final @NotNull List<String> lore;

	/**
	 * constructor with all arguments
	 *
	 * @param material the {@link Material}
	 * @param name     the name / title
	 * @param lore     a {@link List} with all rows of the lore
	 *
	 * @since 1.0.0
	 */
	public HunterItem(
		final @NotNull Material material,
		final @NotNull String name,
		final @NotNull List<String> lore
	) {
		HunterItem.log.trace("Set material: " + material.name());
		this.material = material;
		HunterItem.log.trace("Set name: " + name);
		this.name = name;
		HunterItem.log.trace("Set lore.");
		this.lore = new ArrayList<>(lore);
	}

	/**
	 * checks if this {@link HunterItem} and another are the same
	 *
	 * @param other the other {@link Object} to check
	 *
	 * @return {@code true} if this and other are the same,
	 * 		   {@code false} otherwise
	 *
	 * @see Object#hashCode()
	 *
	 * @since 1.0.0
	 */
	@Override
	public boolean equals(
		final @Nullable Object other
	) {
		HunterItem.log.debug("Checking if two HunterItems are the same...");

		HunterItem.log.trace("Check if other object is null.");
		if (other == null) {
			HunterItem.log.debug("Other object is null, so not the same.");
			return false;
		}

		HunterItem.log.trace("Check if other object is an known type.");
		if (!(other instanceof HunterItem)) {
			HunterItem.log.trace(
				"Other object is not a HunterItem, so not the same."
			);
			return false;
		}

		HunterItem.log.trace("Cast other object as HunterItem.");
		final HunterItem otherItem = (HunterItem)other;

		HunterItem.log.debug("Checking all fields...");
		return this.getMaterial() == otherItem.getMaterial() &&
			   this.getName().equals(otherItem.getName()) &&
			   this.getLore().equals(otherItem.getLore());
	}

	/**
	 * generates a hash code to fast comparison
	 *
	 * @return the calculated semi-unique hash code
	 *
	 * @see Object#hashCode()
	 *
	 * @since 1.0.0
	 */
	@Override
	public int hashCode() {
		HunterItem.log.debug("Generating hash from all fields...");
		return Objects.hash(
			this.getMaterial(),
			this.getName(),
			this.getLore()
		);
	}

	/**
	 * gives a copy of the lore
	 *
	 * @return a {@link List} of rows in the lore
	 *
	 * @since 1.0.0
	 */
	public @NotNull List<String> getLore() {
		HunterItem.log.trace("Create new list with lore.");
		return new ArrayList<>(this.lore);
	}

	/**
	 * adds a row to the lore
	 *
	 * @param row the row to add
	 *
	 * @return {@code true} if the row was successfully added,
	 * 		   {@code false} otherwise
	 *
	 * @see List#add(Object)
	 *
	 * @since 1.0.0
	 */
	public boolean addLore(
		final @NotNull String row
	) {
		HunterItem.log.trace("Add '" + row + "' as new row to lore.");
		return this.lore.add(row);
	}

	/**
	 * removes a row from the lore
	 *
	 * @param row the row to remove
	 *
	 * @return {@code true} if the row was successfully removed,
	 * 		   {@code false} otherwise
	 *
	 * @see List#remove(Object)
	 *
	 * @since 1.0.0
	 */
	public boolean removeLore(
		final @NotNull String row
	) {
		HunterItem.log.trace("Remove row '" + row + "' from lore.");
		return this.lore.remove(row);
	}

	/**
	 * removes a row from the lore, identified by its index
	 *
	 * @param index the index of the row to remove
	 *
	 * @return {@code true} if the row was successfully removed,
	 * 		   {@code false} otherwise
	 *
	 * @see List#remove(int)
	 *
	 * @since 1.0.0
	 */
	public boolean removeLore(
		final int index
	) {
		try {
			HunterItem.log.trace("Remove row " + index + " from lore.");
			this.lore.remove(index);
			return true;
		} catch (IndexOutOfBoundsException exception) {
			HunterItem.log.warn("Row " + index + " not found!", exception);
			return false;
		}
	}

	/**
	 * checks if this {@link HunterItem} and an {@link ItemStack} are equal to
	 * each other
	 *
	 * @param other the {@link ItemStack} to check
	 *
	 * @return {@code true} if both {@link Object}s are equal,
	 * 		   {@code false} otherwise
	 *
	 * @since 1.0.0
	 */
	public boolean equals(
		final @Nullable ItemStack other
	) {
		HunterItem.log.debug(
			"Checking if a HunterItem and an ItemStack are the same..."
		);

		HunterItem.log.trace("Check if other object is null.");
		if (other == null) {
			HunterItem.log.debug("Other object is null, so not the same.");
			return false;
		}

		HunterItem.log.trace(
			"Define name, lore and hide enchantment flag " +
			"from other object with default empty string."
		);
		String       otherName          = "";
		List<String> otherLore          = new ArrayList<>();
		boolean      enchantmentsHidden = false;

		HunterItem.log.trace("Check if other object has ItemMeta.");
		if (other.hasItemMeta()) {
			HunterItem.log.trace("Get ItemMeta from ItemStack.");
			final ItemMeta itemMeta = other.getItemMeta();

			HunterItem.log.trace(
				"Check if ItemMeta from other object is empty."
			);
			if (itemMeta != null) {
				HunterItem.log.trace("Get name from other object.");
				otherName = itemMeta.getDisplayName();

				HunterItem.log.trace("Get lore from other object.");
				List<String> lore = itemMeta.getLore();
				HunterItem.log.trace("Check if other object has a lore.");
				if (itemMeta.hasLore() && lore != null) {
					HunterItem.log.trace("Set lore of other object.");
					otherLore = itemMeta.getLore();
				}

				HunterItem.log.trace(
					"Check if ItemStack has hide enchantment flag set."
				);
				enchantmentsHidden = itemMeta.hasItemFlag(
					ItemFlag.HIDE_ENCHANTS
				);
			}
		}

		HunterItem.log.debug("Checking all fields...");
		return this.getMaterial() == other.getType() &&
			   this.getName().equals(otherName) &&
			   this.getLore().equals(otherLore) &&
			   other.containsEnchantment(HunterItem.ENCHANTMENT) &&
			   other.getEnchantmentLevel(HunterItem.ENCHANTMENT) == 1 &&
			   enchantmentsHidden;
	}

	/**
	 * generates an {@link ItemStack} from this {@link HunterItem}
	 *
	 * @return the generated {@link ItemStack}
	 *
	 * @since 1.0.0
	 */
	public @NotNull ItemStack toItemStack() {
		HunterItem.log.debug("Create ItemStack from HunterItem.");

		HunterItem.log.trace("Define material on ItemStack.");
		final ItemStack itemStack = new ItemStack(this.getMaterial());
		HunterItem.log.trace("Set default Enchantment to ItemStack.");
		itemStack.addUnsafeEnchantment(HunterItem.ENCHANTMENT, 1);

		HunterItem.log.trace("Get ItemMeta from ItemStack.");
		final ItemMeta itemMeta = itemStack.getItemMeta();

		HunterItem.log.trace("Check if ItemMeta ist available.");
		if (itemMeta != null) {
			HunterItem.log.trace("Set display name to ItemMeta.");
			itemMeta.setDisplayName(this.getName());

			HunterItem.log.trace("Set lore to ItemStack.");
			itemMeta.setLore(this.getLore());

			HunterItem.log.trace(
				"Set flag to hide enchantments on ItemStack."
			);
			itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);

			HunterItem.log.trace("Set ItemMeta to ItemStack.");
			itemStack.setItemMeta(itemMeta);
		}

		HunterItem.log.debug("ItemStack fully created.");
		return itemStack;
	}
}
