package world.urelion.easterhunt;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import world.urelion.easterhunt.persistence.EasterEgg;
import world.urelion.easterhunt.persistence.OutputPort;
import world.urelion.easterhunt.persistence.Storage;

/**
 * {@link Listener} to be triggered by a click on an Easter egg
 *
 * @since 1.0.0
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Singleton(style = Style.HOLDER)
public class InteractionListener
implements Listener {
	/**
	 * {@link Singleton} instance of the {@link InteractionListener}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final @NotNull InteractionListener INSTANCE =
		new InteractionListener();

	/**
	 * process hide or seek of an {@link EasterEgg} by clicking on it
	 *
	 * @param event the {@link PlayerInteractEvent}, which is fired on
	 *              interaction with an Easter egg
	 *
	 * @since 1.0.0
	 */
	@EventHandler
	public void onEggInteract(
		final PlayerInteractEvent event
	) {
		InteractionListener.log.trace(
			"Check if event action was a right click on block."
		);
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
			InteractionListener.log.debug(
				"Event action was not a right click on a block. " +
				"Abort event execution."
			);
			return;
		}

		InteractionListener.log.debug("Processing PlayerInteractionEvent...");

		InteractionListener.log.trace(
			"Get player, which interacted with the block."
		);
		final Player player = event.getPlayer();

		InteractionListener.log.trace("Get plugin instance.");
		final EasterHuntPlugin easterHuntPlugin =
			EasterHuntPlugin.getINSTANCE();
		InteractionListener.log.trace(
			"Check if plugin instance is initialized."
		);
		if (easterHuntPlugin == null) {
			InteractionListener.log.error(
				"Plugin not fully loaded! Abort event execution."
			);
			return;
		}

		InteractionListener.log.trace("Get Vault permissions handler.");
		final Permission permissions = easterHuntPlugin.getPermissions();
		if (permissions == null) {
			InteractionListener.log.trace(
				"Send player information about missing permissions handler."
			);
			player.sendMessage(
				"Couldn't check permissions! Is Vault installed?"
			);

			InteractionListener.log.error(
				"Vault permissions handler not initialized! " +
				"Abort event execution."
			);
			return;
		}

		InteractionListener.log.trace("Get ItemStack of interaction.");
		final ItemStack itemStack = event.getItem();

		InteractionListener.log.trace(
			"Check if interaction is done without item."
		);
		if (itemStack == null) {
			InteractionListener.log.trace(
				"No item was used for interaction. Abort execution."
			);
			return;
		}

		InteractionListener.log.trace("Get block of interaction.");
		final Block block = event.getClickedBlock();

		InteractionListener.log.trace("Check if interaction was on a block.");
		if (block == null) {
			InteractionListener.log.debug(
				"Interaction was not an a block. Abort execution."
			);
			return;
		}

		InteractionListener.log.trace("Get location of the interacted block.");
		final Location location = block.getLocation();

		InteractionListener.log.trace(
			"Check which item the player used on interaction."
		);
		if (EasterHuntPlugin.SEEK_ITEM.equals(itemStack)) {
			InteractionListener.log.debug("Player used seek item.");

			if (!(permissions.has(
				player,
				EasterHuntPlugin.COMMAND_NAME + ".egg.seek"
			))) {
				InteractionListener.log.warn(
					"Player " + player.getName() +
					" has no permission to find an Easter egg."
				);
				player.sendMessage(EasterHuntPlugin.MISSING_PERMISSION_TEXT);

				InteractionListener.log.trace(
					"Player has no permission. Abort event execution."
				);
				return;
			}

			final EasterEgg egg;
			try {
				InteractionListener.log.trace(
					"Load Easter egg from store by location."
				);
				egg = Storage.loadEgg(location);
				InteractionListener.log.trace(
					"Check if there is an Easter egg at this location."
				);
			} catch (IllegalStateException exception) {
				InteractionListener.log.error(
					"Couldn't load Easter egg from persistence storage!",
					exception
				);
				return;
			}
			if (egg == null) {
				InteractionListener.log.debug(
					"No easter egg found at " + location + "!"
				);
				return;
			}

			InteractionListener.log.trace(
				"Check if Easter egg was already found."
			);
			if (egg.isFound()) {
				final String message = "This Easter egg was already found.";
				InteractionListener.log.debug(message);
				try {
					player.sendMessage(
						message,
						"There are " + Storage.countNotFoundEggs() + " " +
						ChatColor.GOLD + "Easter egg(s)" + ChatColor.RESET +
						" left in the world."
					);
				} catch (IllegalStateException exception) {
					InteractionListener.log.error(
						"Couldn't load statistics from persistence storage!",
						exception
					);
				}

				InteractionListener.log.trace("End event execution.");
				return;
			}

			try {
				InteractionListener.log.debug("Mark Easter egg as found.");
				egg.setFoundBy(player.getUniqueId());

				InteractionListener.log.debug(
					"Send message to player about an Easter egg found."
				);
				player.sendMessage(
					"Congratulations, you found an " +
					ChatColor.GOLD + "Easter egg" + ChatColor.RESET + "!",
					"You found " + Storage.countEggs(player.getUniqueId()) +
					" Easter egg(s) in total.",
					"There are " + Storage.countNotFoundEggs() + " " +
					ChatColor.GOLD + "Easter egg(s)" + ChatColor.RESET +
					" left in the world."
				);
			} catch (IllegalStateException exception) {
				InteractionListener.log.error(
					"Couldn't store Easter egg as found!",
					exception
				);
			}
		} else if (EasterHuntPlugin.HIDE_ITEM.equals(itemStack)) {
			InteractionListener.log.debug("Player used hide item.");

			InteractionListener.log.trace("Check if block is a lever.");
			if (block.getType() == Material.LEVER) {
				InteractionListener.log.trace(
					"Check if player has permission to define an output port."
				);
				if (!(permissions.has(
					player,
					EasterHuntPlugin.COMMAND_NAME + "output.set"
				))) {
					InteractionListener.log.warn(
						"Player " + player.getName() +
						" has no permission to define an output port.");
					player.sendMessage(
						EasterHuntPlugin.MISSING_PERMISSION_TEXT
					);

					InteractionListener.log.trace(
						"Player has no permission. Abort event execution."
					);
					return;
				}

				try {
					InteractionListener.log.debug("Store an output port.");
					Storage.storeOutputPort(new OutputPort(location));
				} catch (IllegalStateException exception) {
					InteractionListener.log.error(
						"Couldn't store Easter egg! " +
						"Plugin is not fully initialized!",
						exception
					);
				}

				InteractionListener.log.debug(
					"Definition of output port finished. Stop event execution."
				);
				event.setCancelled(true);

				InteractionListener.log.trace(
					"Send player information about created output port."
				);
				player.sendMessage(
					"New " +
					ChatColor.RED + "output port" + ChatColor.RESET +
					" set."
				);

				InteractionListener.log.trace("End event execution.");
				return;
			}

			if (!(permissions.has(
				player,
				EasterHuntPlugin.COMMAND_NAME + ".egg.hide"
			))) {
				InteractionListener.log.warn(
					"Player " + player.getName() +
					" has no permission to hide an Easter egg."
				);
				player.sendMessage(EasterHuntPlugin.MISSING_PERMISSION_TEXT);

				InteractionListener.log.trace(
					"Player has no permission. Abort event execution."
				);
				return;
			}

			try {
				InteractionListener.log.trace(
					"Load Easter egg from persistence storage."
				);
				final EasterEgg egg = Storage.loadEgg(location);
				InteractionListener.log.trace(
					"Check if there is an Easter egg at this location."
				);
				if (egg != null) {
					final String message =
						"This Easter egg is already hidden.";
					InteractionListener.log.debug(message);
					player.sendMessage(message);
					return;
				}

				InteractionListener.log.debug("Store Easter egg.");
				Storage.storeEgg(new EasterEgg(location));
			} catch (IllegalStateException exception) {
				InteractionListener.log.error(
					"Couldn't store Easter egg! " +
					"Plugin is not fully initialized!",
					exception
				);
				return;
			}

			try {
				InteractionListener.log.debug(
					"Send message to player about an Easter egg hide."
				);
				player.sendMessage(
					"Easter egg was hidden.",
					"--- Statistics ---",
					"- Easter egg(s) in total: " + Storage.countAllEggs(),
					"- Easter egg(s) found: " + Storage.countFoundEggs(),
					"- Easter egg(s) left: " + Storage.countNotFoundEggs()
				);
			} catch (IllegalStateException exception) {
				InteractionListener.log.error(
					"Couldn't load statistics from persistence storage!",
					exception
				);
			}
		}
	}
}
