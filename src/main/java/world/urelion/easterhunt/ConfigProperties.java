package world.urelion.easterhunt;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.bukkit.configuration.file.FileConfiguration;
import org.hibernate.Hibernate;
import org.jetbrains.annotations.NotNull;
import world.urelion.easterhunt.persistence.EasterEgg;

import java.net.URL;

/**
 * contains fix names or path of properties in the {@link FileConfiguration}
 *
 * @since 1.0.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigProperties {
	/**
	 * property path in {@link FileConfiguration} of the threshold,
	 * when all {@link EasterEgg}s are defined as found
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String FOUND_ALL_THRESHOLD_PROPERTY =
		"foundAllThreshold";

	/**
	 * full-qualified {@link Class} of the database driver
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String DB_DRIVER       = "db.driver";
	/**
	 * full-qualified {@link Class} of the {@link Hibernate} dialect
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String DB_DIALECT      = "db.dialect";
	/**
	 * property path of the JDBC {@link URL}
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String DB_URL          = "db.url";
	/**
	 * property path of the username for the database
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String DB_USERNAME     = "db.username";
	/**
	 * property path of the password for the database
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String DB_PASSWORD     = "db.password";
	/**
	 * property path of the table prefix
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String DB_TABLE_PREFIX = "db.tablePrefix";

	/**
	 * should all hidden Easter eggs be shown on Dynmap
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String MAP_SHOW_HIDDEN = "map.showHidden";

	/**
	 * should all found Easter eggs be shown on Dynmap
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String MAP_SHOW_FOUND = "map.showFound";
}
