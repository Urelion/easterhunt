package world.urelion.easterhunt.hook;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import world.urelion.easterhunt.EasterHuntPlugin;
import world.urelion.easterhunt.persistence.Storage;

import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * defines a new placeholder to get the leaderboard of the Easter eggs search.
 *
 * @since 1.0.0
 */
@Slf4j
public class LeaderboardExpansion
extends PlaceholderExpansion {
	/**
	 * an unique random identifier for {@link LeaderboardExpansion#hashCode()}
	 *
	 * @since 1.0.0
	 */
	@Getter(AccessLevel.PRIVATE)
	private final double uniqueIdentifier = Math.random();

	/**
	 * generates an unique hash code value
	 *
	 * @return the hash code value
	 *
	 * @see Object#hashCode()
	 *
	 * @since 1.0.0
	 */
	@Override
	public int hashCode() {
		return Objects.hash(
			this.getIdentifier(),
			this.getAuthor(),
			this.getVersion(),
			this.getUniqueIdentifier()
		);
	}

	/**
	 * gives the identifier of this {@link PlaceholderExpansion}
	 *
	 * @return the {@link String} to identify this {@link PlaceholderExpansion}
	 *
	 * @see PlaceholderExpansion#getIdentifier()
	 *
	 * @since 1.0.0
	 */
	@Override
	public @NotNull String getIdentifier() {
		LeaderboardExpansion.log.trace(
			"Use shorten command name as identifier for placeholder."
		);
		return EasterHuntPlugin.COMMAND_NAME.substring(0, 5);
	}

	/**
	 * gives the author of this {@link Plugin}
	 *
	 * @return the author if this {@link Plugin}
	 *
	 * @see PlaceholderExpansion#getAuthor()
	 *
	 * @since 1.0.0
	 */
	@Override
	public @NotNull String getAuthor() {
		LeaderboardExpansion.log.trace("Get plugin instance.");
		EasterHuntPlugin easterHuntPlugin = EasterHuntPlugin.getINSTANCE();
		LeaderboardExpansion.log.trace("Check if plugin is initialized.");
		if (easterHuntPlugin == null) {
			LeaderboardExpansion.log.error(
				"Plugin is not initialized successfully! " +
				"Use team name as placeholder author."
			);
			return "Urelion-Team";
		}

		LeaderboardExpansion.log.trace(
			"Return plugin author(s) as placeholder author(s)."
		);
		return String.join(
			", ",
			easterHuntPlugin.getDescription().getAuthors()
		);
	}

	/**
	 * gives the version of this {@link Plugin}
	 *
	 * @return the version of this {@link Plugin}
	 *
	 * @see PlaceholderExpansion#getVersion()
	 *
	 * @since 1.0.0
	 */
	@Override
	public @NotNull String getVersion() {
		LeaderboardExpansion.log.trace("Get plugin instance.");
		EasterHuntPlugin easterHuntPlugin = EasterHuntPlugin.getINSTANCE();
		LeaderboardExpansion.log.trace("Check if plugin is initialized.");
		if (easterHuntPlugin == null) {
			String zeroVersion = "0.0.0";
			LeaderboardExpansion.log.error(
				"Plugin not initialized! Return zero version: " + zeroVersion
			);
			return zeroVersion;
		}

		LeaderboardExpansion.log.trace("Return version of the plugin.");
		return easterHuntPlugin.getDescription().getVersion();
	}

	/**
	 * requests a placeholder replacement
	 *
	 * @param player the {@link OfflinePlayer} context
	 * @param params the placeholder to replace
	 *
	 * @return the replaced placeholder or
	 *		   {@code null} if an invalid placeholder was requested
	 *
	 * @see PlaceholderExpansion#onRequest(OfflinePlayer, String)
	 *
	 * @since 1.0.0
	 */
	@Override
	public @Nullable String onRequest(
		final OfflinePlayer player,
		final @NotNull String params
	) {
		LeaderboardExpansion.log.trace("Define RegEx group name of the rank.");
		final String rankGroup = "rank";
		LeaderboardExpansion.log.trace("Define RegEx group of the type.");
		final String typeGroup = "type";
		LeaderboardExpansion.log.trace("Define RegEx of placeholder.");
		final String regEx =
			"(?<" + rankGroup + ">[1-9]\\d*)(_(?<" +
			typeGroup + ">(na(me)?|am(ount)?)))?";
		LeaderboardExpansion.log.trace("Get RegEx pattern of placeholder.");
		final Pattern pattern = Pattern.compile(regEx);
		LeaderboardExpansion.log.trace("Get RegEx matcher of placeholder.");
		final Matcher matcher = pattern.matcher(params);

		LeaderboardExpansion.log.trace("Check if placeholder matches RegEx.");
		if (!(matcher.matches())) {
			LeaderboardExpansion.log.debug(
				"Placeholder doesn't match RegEx. Cancel placeholder request."
			);
			return null;
		}

		LeaderboardExpansion.log.trace("Get rank from placeholder.");
		final String rankString = matcher.group(rankGroup);
		LeaderboardExpansion.log.trace("Define rank number.");
		final int rank;
		try {
			LeaderboardExpansion.log.trace("Parse rank as number.");
			rank = Integer.parseInt(rankString);
		} catch (NumberFormatException exception) {
			LeaderboardExpansion.log.error(
				"Rank is not a number! Cancel placeholder request.",
				exception
			);
			return null;
		}

		LeaderboardExpansion.log.trace("Get type from placeholder.");
		final String type = matcher.group(typeGroup);

		LeaderboardExpansion.log.trace("Define finders list.");
		final LinkedHashMap<UUID, Integer> finders;
		try {
			LeaderboardExpansion.log.trace(
				"Get finders list from persistence storage."
			);
			finders = Storage.getFinders();
		} catch (IllegalStateException exception) {
			LeaderboardExpansion.log.error(
				"Persistence storage is not initialized! " +
				"Cancel placeholder request.",
				exception
			);
			return null;
		}

		LeaderboardExpansion.log.trace("Check if rank exists.");
		if (finders.size() >= rank) {
			LeaderboardExpansion.log.trace("Get plugin instance.");
			final EasterHuntPlugin easterHuntPlugin =
				EasterHuntPlugin.getINSTANCE();
			LeaderboardExpansion.log.trace("Check if plugin is initialized.");
			if (easterHuntPlugin == null) {
				LeaderboardExpansion.log.error(
					"Plugin not initialized successfully!"
				);
				return null;
			}

			LeaderboardExpansion.log.trace(
				"Get the UUID of the requested finder."
			);
			final UUID finderId = finders.keySet().toArray(
				new UUID[0]
			)[rank - 1];
			LeaderboardExpansion.log.trace(
				"Define name of the requested finder."
			);
			final String finderName;
			LeaderboardExpansion.log.trace("Get player by finder identifier.");
			OfflinePlayer finder =
				easterHuntPlugin.getServer().getOfflinePlayer(finderId);

			LeaderboardExpansion.log.trace(
				"Check if player with finder identifier exists."
			);
			finderName = finder.getName();
			LeaderboardExpansion.log.trace(
				"Found player " + finderName +
				" with UUID " + finderId + "."
			);

			LeaderboardExpansion.log.trace("Get amount of found Easter eggs.");
			final int amountFound = finders.get(finderId);
			LeaderboardExpansion.log.trace("Check if type argument is given.");
			if (type != null && !(type.isEmpty())) {
				if (type.startsWith("na")) {
					LeaderboardExpansion.log.trace(
						"Return name of the finder."
					);
					return finderName;
				} else if (type.startsWith("am")) {
					LeaderboardExpansion.log.trace(
						"Return amount of found Easter eggs."
					);
					return String.valueOf(amountFound);
				}
			}

			LeaderboardExpansion.log.trace("Return placeholder content.");
			return finderName + ": " + amountFound;
		} else {
			LeaderboardExpansion.log.trace("Define empty placeholder.");
			final String emptyPlaceholder = "-";
			LeaderboardExpansion.log.debug(
				"Rank #" + rank +
				" doesn't exist! Return empty placeholder "
				+ emptyPlaceholder + "."
			);
			return emptyPlaceholder;
		}
	}
}
