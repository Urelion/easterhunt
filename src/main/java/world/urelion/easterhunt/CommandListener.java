package world.urelion.easterhunt;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import world.urelion.easterhunt.persistence.Storage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * {@link CommandExecutor} to execute the {@link EasterHuntPlugin#COMMAND_NAME}
 * {@link Command}
 *
 * @since 1.0.0
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Singleton(style = Style.HOLDER)
public class CommandListener
implements CommandExecutor, TabExecutor {
	/**
	 * {@link Singleton} instance of {@link CommandListener}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final @NotNull CommandListener INSTANCE = new CommandListener();

	/**
	 * Execute on {@link Command} and handle {@link Command}
	 * {@link EasterHuntPlugin#COMMAND_NAME}
	 *
	 * @param sender  the executor, which starts the {@link Command}
	 * @param command the {@link Command}, which was started by the sender
	 * @param label   the label
	 * @param args    the sent {@link Command} arguments
	 *
	 * @return {@code true} if execution was successful,
	 * 		   {@code false} otherwise
	 *
	 * @see CommandExecutor#onCommand(CommandSender, Command, String, String[])
	 *
	 * @since 1.0.0
	 */
	@Override
	public boolean onCommand(
		final @NotNull CommandSender sender,
		final @NotNull Command command,
		final @NotNull String label,
		final @NotNull String[] args
	) {
		CommandListener.log.trace("Get command name.");
		final String commandName = command.getName();

		CommandListener.log.debug("Processing command " + commandName + "...");

		CommandListener.log.trace("Get plugin instance.");
		final EasterHuntPlugin easterHuntPlugin =
			EasterHuntPlugin.getINSTANCE();
		CommandListener.log.trace("Check if plugin instance is initialized.");
		if (easterHuntPlugin == null) {
			CommandListener.log.error(
				"Plugin not fully loaded! Abort command execution."
			);
			return false;
		}

		CommandListener.log.trace("Get Vault permissions handler.");
		final Permission permissions = easterHuntPlugin.getPermissions();
		if (permissions == null) {
			CommandListener.log.trace(
				"Send command sender information " +
				"about missing permissions handler."
			);
			sender.sendMessage(
				"Couldn't check permissions! Is Vault installed?"
			);

			CommandListener.log.error(
				"Vault permissions handler not initialized! " +
				"Abort command execution."
			);
			return true;
		}

		if (!(permissions.has(
			sender,
			EasterHuntPlugin.COMMAND_NAME + ".command"
		))) {
			CommandListener.log.warn(
				"Command sender " + sender.getName() +
				" has no permission to execute " +
				EasterHuntPlugin.COMMAND_NAME + " command."
			);
			sender.sendMessage(EasterHuntPlugin.MISSING_PERMISSION_TEXT);

			CommandListener.log.trace(
				"Sender has no permission. Abort command execution."
			);
			return true;
		}

		CommandListener.log.trace("Check if command is bound to this plugin.");
		if (EasterHuntPlugin.COMMAND_NAME.equalsIgnoreCase(commandName)) {
			CommandListener.log.trace("Check if sub-command was set.");
			if (args.length < 1) {
				CommandListener.log.debug(
					"No sub-command was given! Abort command execution."
				);
				return false;
			}

			final String subCommand = args[0];
			Player       player;

			CommandListener.log.trace(
				"Check if player was given in arguments."
			);
			if (args.length >= 2) {
				if ("all".equalsIgnoreCase(args[1])) {
					if (!(permissions.has(
						sender, (
							EasterHuntPlugin.COMMAND_NAME +
							".command." + subCommand + ".all"
						).toLowerCase(EasterHuntPlugin.DEFAULT_LOCALE))
					)) {
						CommandListener.log.debug(
							"Inform sender about missing permission."
						);
						sender.sendMessage(
							EasterHuntPlugin.MISSING_PERMISSION_TEXT
						);

						CommandListener.log.trace(
							"Missing permission! Abort command execution."
						);
						return true;
					}

					ItemStack item;
					CommandListener.log.trace(
						"Check which item should be given."
					);
					if ("hide".equalsIgnoreCase(subCommand)) {
						item = EasterHuntPlugin.HIDE_ITEM.toItemStack();
					} else {
						item = EasterHuntPlugin.SEEK_ITEM.toItemStack();
					}
					CommandListener.log.trace(
						"Iterate over all online players."
					);
					for (
						Player onlinePlayer :
						easterHuntPlugin.getServer().getOnlinePlayers()
					) {
						CommandListener.log.trace(
							"Give player " + onlinePlayer.getName() +
							" the item to " + subCommand + " Easter eggs."
						);
						onlinePlayer.getInventory().addItem(item);
					}

					CommandListener.log.debug(
						"Gave all online players the item. " +
						"End command execution."
					);
					return true;
				}

				CommandListener.log.trace(
					"Get player by name from arguments."
				);
				player = Bukkit.getPlayer(args[1]);
			} else if (sender instanceof Player) {
				CommandListener.log.trace(
					"Use command sender as item receiver."
				);
				player = (Player)sender;
			} else {
				CommandListener.log.debug(
					"CommandSender is not a player " +
					"and no player name was given in arguments! " +
					"Abort command execution."
				);
				return false;
			}

			CommandListener.log.trace("Check if a valid player was found.");
			if (player == null) {
				CommandListener.log.debug(
					"No player found to send the items to! " +
					"Abort command execution."
				);
				return false;
			}

			CommandListener.log.trace(
				"Sub-command is " + subCommand + ". Get players inventory."
			);
			final Inventory inventory = player.getInventory();

			CommandListener.log.trace("Switch to requested sub-command.");
			if ("seek".equalsIgnoreCase(subCommand)) {
				CommandListener.log.trace(
					"Check if command sender has the necessary permission."
				);
				if ((sender.equals(player) && (!(permissions.has(
					sender,
					EasterHuntPlugin.COMMAND_NAME +
					".command.seek.self"
				)))) || (!(permissions.has(
					sender,
					EasterHuntPlugin.COMMAND_NAME +
					".command.seek.other"
				)))) {
					CommandListener.log.debug(
						"Inform sender about missing permission."
					);
					sender.sendMessage(
						EasterHuntPlugin.MISSING_PERMISSION_TEXT
					);

					CommandListener.log.trace(
						"Missing permission! Abort command execution."
					);
					return true;
				}

				CommandListener.log.debug(
					"Give player " + player.getName() + " seek item."
				);
				inventory.addItem(EasterHuntPlugin.SEEK_ITEM.toItemStack());
			} else if ("hide".equalsIgnoreCase(subCommand)) {
				CommandListener.log.trace(
					"Check if command sender has the necessary permission."
				);
				if ((sender.equals(player) && (!(permissions.has(
					sender,
					EasterHuntPlugin.COMMAND_NAME +
					".command.hide.self"
				)))) || (!(permissions.has(
					sender,
					EasterHuntPlugin.COMMAND_NAME +
					".command.hide.other"
				)))) {
					CommandListener.log.debug(
						"Inform sender about missing permission."
					);
					sender.sendMessage(
						EasterHuntPlugin.MISSING_PERMISSION_TEXT
					);

					CommandListener.log.trace(
						"Missing permission! Abort command execution."
					);
					return true;
				}

				CommandListener.log.debug(
					"Give player " + player.getName() + " hide item."
				);
				inventory.addItem(EasterHuntPlugin.HIDE_ITEM.toItemStack());
			} else if (
				"stats".equalsIgnoreCase(subCommand) ||
				"statistics".equalsIgnoreCase(subCommand)
			) {
				CommandListener.log.trace(
					"Check if command sender has the necessary permission."
				);
				if ((sender.equals(player) && (!(permissions.has(
					sender,
					EasterHuntPlugin.COMMAND_NAME +
					".command.stats.self"
				)))) || (!(permissions.has(
					sender,
					EasterHuntPlugin.COMMAND_NAME +
					".command.stats.other"
				)))) {
					CommandListener.log.debug(
						"Inform sender about missing permission."
					);
					sender.sendMessage(
						EasterHuntPlugin.MISSING_PERMISSION_TEXT
					);

					CommandListener.log.trace(
						"Missing permission! Abort command execution."
					);
					return true;
				}

				try {
					CommandListener.log.trace("Get string of player name.");
					String playerName = player.getName();
					CommandListener.log.trace("Define message string.");
					String message =
						" found " +
						Storage.countEggs(player.getUniqueId()) +
						" Easter eggs.";
					CommandListener.log.trace(playerName + message);
					sender.sendMessage(
						(sender.equals(player) ? "You" : playerName) +
						message
					);
				} catch (IllegalStateException exception) {
					CommandListener.log.error(
						"Couldn't get statistics from persistence storage!",
						exception
					);
				}
			} else {
				String errorMessage = "Unknown subcommand: " + subCommand;
				CommandListener.log.warn(errorMessage);
				sender.sendMessage(errorMessage);

				return false;
			}

			CommandListener.log.debug(
				"Command " + commandName + " processed successfully."
			);
			return true;
		}

		CommandListener.log.debug(
			"Command " + commandName +
			" is not bound to this plugin. Skip command execution."
		);
		return true;
	}

	/**
	 * provides argument suggestions to assist {@link Player}
	 *
	 * @param sender  the executor, which starts the {@link Command}
	 * @param command the {@link Command}, which was started by the sender
	 * @param label   the label
	 * @param args    the sent {@link Command} arguments
	 *
	 * @return {@code List} of argument suggestions
	 *
	 * @see TabExecutor#onTabComplete(CommandSender, Command, String, String[])
	 *
	 * @since 1.0.0
	 */
	@Override
	public @Nullable List<String> onTabComplete(
		final @NotNull CommandSender sender,
		final @NotNull Command command,
		final @NotNull String label,
		final @NotNull String[] args
	) {
		CommandListener.log.trace("Get command name.");
		final String commandName = command.getName();

		CommandListener.log.trace("Create list of filtered arguments.");
		final List<String> arguments = new ArrayList<>();
		{
			CommandListener.log.trace("Create temporary list of arguments.");
			final List<String> argsList = Arrays.asList(args);
			CommandListener.log.trace(
				"Reserve order of arguments in temporary list."
			);
			Collections.reverse(argsList);
			CommandListener.log.trace(
				"Define flag of first non-empty argument."
			);
			boolean firstNonEmpty = false;
			CommandListener.log.trace("Iterate over all arguments.");
			for (String arg : argsList) {
				CommandListener.log.trace(
					"Check if argument is empty or not at the end of list."
				);
				if (firstNonEmpty || (arg != null && !(arg.isEmpty()))) {
					CommandListener.log.trace(
						"Add argument " + arg + " to filtered list."
					);
					arguments.add(arg);
					CommandListener.log.trace(
						"Set end of non-empty arguments."
					);
					firstNonEmpty = true;
				}
			}
			CommandListener.log.trace(
				"Reverse order of filtered list to get right order."
			);
			Collections.reverse(arguments);
		}

		CommandListener.log.debug(
			"Getting suggested arguments for command '/" + commandName +
			" " + String.join(" ", arguments) + "' ..."
		);

		CommandListener.log.trace("Check if command is bound to this plugin.");
		if (!(EasterHuntPlugin.COMMAND_NAME.equalsIgnoreCase(
			command.getName()
		))) {
			CommandListener.log.trace(
				"Command " + commandName + " is not bound to this plugin."
			);
			return null;
		}

		CommandListener.log.trace("Create new empty suggestion list.");
		List<String> suggestions = new ArrayList<>();

		CommandListener.log.trace(
			"Check on which level of arguments, suggestions are requested."
		);
		if (arguments.size() <= 0) {
			CommandListener.log.trace(
				"Add first level suggestions: <seek|hide|stats>"
			);
			suggestions.add("seek");
			suggestions.add("hide");
			suggestions.add("stats");
		} else if (arguments.size() <= 1) {
			CommandListener.log.trace("Add argument 'all' to suggestions.");
			suggestions.add("all");

			CommandListener.log.trace("Create list of player names.");
			final List<String> playerList = new ArrayList<>();
			CommandListener.log.trace("Iterate over all online players.");
			for (Player player : Bukkit.getOnlinePlayers()) {
				CommandListener.log.trace("Get players name.");
				playerList.add(player.getName());
			}
			CommandListener.log.trace("Sort names of players.");
			Collections.sort(playerList);
			CommandListener.log.trace(
				"Added all player names to suggestions."
			);
			suggestions.addAll(playerList);
		}

		return suggestions;
	}
}
